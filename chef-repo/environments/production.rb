name "production"
description "for production!"

  cookbook "apache", "= 0.2.0"
  cookbook "chef-client", "= 4.3.0"
  cookbook "logrotate", "= 1.9.1"
  cookbook "cron", "= 1.6.1"


default_attributes(
"chef_client" => {
  "interval" => 300
})