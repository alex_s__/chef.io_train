# See https://docs.chef.io/config_rb_knife.html for more information on knife configuration options

current_dir = File.dirname(__FILE__)
log_level                :info
log_location             STDOUT
node_name                "epamlexgmailcom"
client_key               "#{current_dir}/epamlexgmailcom.pem"
validation_client_name   "nasa0-validator"
validation_key           "#{current_dir}/nasa0-validator.pem"
chef_server_url          "https://api.opscode.com/organizations/nasa0"
cookbook_path            ["#{current_dir}/../cookbooks"]
